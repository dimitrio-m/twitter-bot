// index.js
require('dotenv').config();
var request = require('request');
var cheerio = require('cheerio');
var Twitter = require('twitter');
var mongoose = require('mongoose');
var CronJob = require('cron').CronJob;
var ShorteST = require('shorte.st');
var shortest = new ShorteST("77185aa78d44a6c6f12853e6c6f66791");

var dia = "10Jul";

var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

mongoose.connect(process.env.MONGO_URI);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connection to db succesful!");
});

var postSchema = mongoose.Schema({
    title: String,
    category: String,
    url: String,
    shortened: Boolean,
    published: Date,
    created: Date
});

postSchema.methods.tweet = function () {
  var post = this;
  client.post('statuses/update', {status: this.title + " " + this.url },  function(error, tweet, response){
    if(error) {
      console.log(error);
      if(error){
        post.category = dia;
        client.post('statuses/update', {status: post.title + " " + post.url + " #" + post.category},  function(error, tweet, response){
          if(error)
            console.error(error);

          console.log(new Date().toString() + " I just tweeted: " + post.title);  // Tweet body.

        });
      }
    }
    else console.log(new Date().toString() + " I just tweeted: " + post.title);  // Tweet body.
  });
}

var Post = mongoose.model('Post', postSchema);

var getUrls = function() {
  request('http://www.genbeta.com', function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      $('h2.article-home-header').each(function(i, element){
        // Select the previous element
        var a = $(this).children();
        // Get the rank by parsing the element two levels above the "a" element
        var category = a.parent().prev().children().text();
        // Parse the link title
        var title = a.text();
        if (title.length > 100) title = title.slice(0, 95).concat("...");
        // Parse the href attribute from the "a" element
        var url = a.attr('href');

        Post.find({title: title}, function(err, post) {

          if(err) return console.error("Hubo un error al buscar: " + title + "\n" + err);

          if(post.length === 0) {
            var date = new Date();
            date.setHours(date.getHours() - 3);
            var post = new Post({
              title: title,
              category: category.replace(/ /g, ""),
              url: url,
              shortened: false,
              published: date,
              created: new Date()
            });

            post.save(function() {
              if (err) return console.error(err);
              else console.log("Post guardado.");
            });
          }
        });
      });

    }
  });
}

var getSalud = function() {
  request('http://www.noticias24.com/salud', function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      $('div.article').each(function(i, element){
        // Select the previous element
        var a = $(this).children('h1').children();
        // Get the rank by parsing the element two levels above the "a" element
        var category = "Vive";
        // Parse the link title
        var title = a.text();
        if (title.length > 100) title = title.slice(0, 95).concat("...");
        // Parse the href attribute from the "a" element
        var url = a.attr('href');

        Post.find({title: title}, function(err, post) {

          if(err) return console.error("Hubo un error al buscar: " + title + "\n" + err);

          if(post.length === 0) {
            var date = new Date();
            date.setHours(date.getHours() - 3);
            var post = new Post({
              title: title,
              category: category,
              url: url,
              shortened: false,
              published: date,
              created: new Date()
            });

            post.save(function() {
              if (err) return console.error(err);
              else console.log("Post guardado.");
            });
          }
        });
      });

    }
  });
}

var getUrlsPol = function() {
  request('http://lasampablera.com', function (error, response, html) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(html);

      var groups = $("div.medium-8-new");
      $(groups['0']).find(".feed-content").each(function(i, element){
        // Select the previous element
        var a = $(this).children('a');
        // Get the rank by parsing the element two levels above the "a" element
        var category = "";
        // Parse the link title
        var title = a.text();
        if (title.length > 100) title = title.slice(0, 95).concat("...");
        // Parse the href attribute from the "a" element
        var url = a.attr('href');

        Post.find({title: title}, function(err, post) {

          if(err) return console.error("Hubo un error al buscar: " + title + "\n" + err);

          if(post.length === 0) {
            var date = new Date();
            date.setHours(date.getHours() - 3);
            var post = new Post({
              title: title,
              category: category.replace(/ /g, ""),
              url: url,
              shortened: false,
              published: date,
              created: new Date()
            });

            post.save(function() {
              if (err) return console.error(err);
              else console.log("Post guardado.");
            });
          }
        });
      });
      $(groups['2']).find(".feed-content").each(function(i, element){
        // Select the previous element
        var a = $(this).children('a');
        // Get the rank by parsing the element two levels above the "a" element
        var category = "";
        // Parse the link title
        var title = a.text();
        if (title.length > 100) title = title.slice(0, 95).concat("...");
        // Parse the href attribute from the "a" element
        var url = a.attr('href');

        Post.find({title: title}, function(err, post) {

          if(err) return console.error("Hubo un error al buscar: " + title + "\n" + err);

          if(post.length === 0) {
            var date = new Date();
            date.setHours(date.getHours() - 3);
            var post = new Post({
              title: title,
              category: category.replace(/ /g, ""),
              url: url,
              shortened: false,
              published: date,
              created: new Date()
            });

            post.save(function() {
              if (err) return console.error(err);
              else console.log("Post guardado.");
            });
          }
        });
      });
    }
  });
}

var tweet = function tweet() {
  console.log("Intentare Twittear");
  var date = new Date();
  date.setHours(date.getHours()-3);
  Post.where('published').lte(date).exec(function(err, posts) {
      if (err) return console.error(err);
      if(posts.length === 0) return console.error("Posts vacios");
      var post = posts.pop();
      while(post.shortened) {
        post = posts.pop();
      }
      console.log(posts.length);
      if(!post.shortened) {
        console.log(post);
        shortest.link(post.url, function(err, url) {
           if (err) return console.error(err);
           console.log("Short URL is: "+url);
           post.url = url;
           post.shortened = true;
           post.published = new Date();
           post.save(function() {
             if (err) return console.error(err);
             else console.log("Post modificado con url.");
             post.tweet();
           });
        });
      }
      else {
        post.published = new Date();
        post.save(function() {
          if (err) return console.error(err);
          else console.log("Post ya republicado.");
          post.tweet();

        });
      }
  });
}

var cleanUrl = function() {
  console.log("Limpiando links viejos");
  var date = new Date();
  date.setDate(date.getDate()-1);
  Post.where('created').lte(date).exec(function(err, posts) {
      if (err) return console.error(err);
      if(posts.length === 0) return console.error("No hay posts viejos");
      posts.forEach(function(post){
        Post.remove(post, function (err) {
          if (err) return console.error(err);
        });
      });
  });
}

getUrls();
getUrlsPol();
getSalud();
tweet();

Post.find(function (err, kittens) {
  if (err) return console.error(err);
  console.log(kittens.length);
});
var tweetingJob = new CronJob('0 */15 * * * *', tweet, function () {
    console.log("Se subio un tweet");
  },
  false, /* Start the job right now */
  'America/Los_Angeles' /* Time zone of this job. */
);

var urlsJob = new CronJob('00 00 */1 * * *', getUrls, function () {
    console.log("Se actualizo la lista de posts");
    getUrlsPol();
    getSalud();
  },
  false, /* Start the job right now */
  'America/Los_Angeles' /* Time zone of this job. */
);

var cleanUrls = new CronJob('00 00 00 * * *', cleanUrl, function () {
    console.log("Se actualizo la lista de posts");
  },
  false, /* Start the job right now */
  'America/Los_Angeles' /* Time zone of this job. */
);

//cleanUrls.start();
urlsJob.start();
tweetingJob.start();
